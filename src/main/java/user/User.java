package user;

import channel.MyChannel;
import chat.State;
import com.google.common.eventbus.EventBus;
import org.jgroups.JChannel;
import org.jgroups.Message;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by adakis on 2017-03-23.
 */
public class User {
    private String nickname;
    private Map<String, JChannel> connectedChannels;


    public User(String nickname) {
         connectedChannels = new HashMap<String, JChannel>();
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Map<String, JChannel> getConnectedChannels() {
        return connectedChannels;
    }

    public void setConnectedChannels(Map<String, JChannel> connectedChannels) {
        this.connectedChannels = connectedChannels;
    }

    public void addChannel(MyChannel channel) {

    }

    public void joinChannel(EventBus eventBus, MyChannel channel){
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Type channel name:");
            String channelName = scanner.nextLine();

            if (connectedChannels.containsKey(channelName)) {
                System.out.println("Already connected to that channel.");
                return;
            }
            channel.join(this, channelName, eventBus);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showChannels(State state){
        for (Map.Entry<String, Collection<String>> actionsEntry : state.channelsWithUsers().entrySet()) {
            System.out.println("Channel: " + actionsEntry.getKey());
            for (String nick : actionsEntry.getValue()) {
                System.out.println("\t" + nick);
            }
        }
    }

    public void cleanUp(MyChannel myChannel) {
        for (Map.Entry<String, JChannel> channel : connectedChannels.entrySet()) {
            try {
                myChannel.getChatManager().sendLeaveMessage(nickname, channel.getKey());
                channel.getValue().close();
                myChannel.getChannelWriters().remove(channel.getKey());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            myChannel.getChatManager().cleanUp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void putChannel(String channelName, JChannel channel) {
        connectedChannels.put(channelName, channel);
    }

    public void sendMessage() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please insert channel name:");
        String channelName = scanner.nextLine();
        if (channelName.isEmpty() || !connectedChannels.containsKey(channelName)) {
            System.out.println("Unknown channel");
            return;
        }

        System.out.println("Message:");
        String msgContent = scanner.nextLine();
        while (msgContent.isEmpty()) {
            System.out.println("Cannot send empty message!");
            msgContent = scanner.nextLine();
        }

        Message message = new Message(null, null,
                ChatOperationProtos.ChatMessage.newBuilder().setMessage(msgContent).build().toByteArray()
        );

        try {
            connectedChannels.get(channelName).send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}