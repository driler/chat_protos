package message;

/**
 * Created by adakis on 2017-03-24.
 */
public class UserJoinMessage implements Message {

    private final String channel;
    private final String nickname;

    public UserJoinMessage(String channel, String nickname) {
        this.channel = channel;
        this.nickname = nickname;
    }

    @Override
    public String getChannel() {
        return channel;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public String toString() {
        return "<" + nickname + "> joined";
    }
}
