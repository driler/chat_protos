package message;

/**
 * Created by adakis on 2017-03-24.
 */
public interface Message {
    String getChannel();
    String getNickname();
}
