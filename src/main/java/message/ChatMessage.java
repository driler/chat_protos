package message;

/**
 * Created by adakis on 2017-03-24.
 */
public class ChatMessage implements Message {

    private final String channel;
    private final String nickname;
    private final String content;

    public ChatMessage(String channel, String nickname, String content) {
        this.channel = channel;
        this.nickname = nickname;
        this.content = content;
    }

    @Override
    public String getChannel() {
        return channel;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    public String content() {
        return content;
    }

    @Override
    public String toString() {
        return "<" + nickname + ">: " + content;
    }
}
