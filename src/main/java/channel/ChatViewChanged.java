package channel;

import java.util.Collection;
import java.util.Map;

/**
 * Created by adakis on 2017-03-24.
 */
public class ChatViewChanged {

    public ChatViewChanged(Map<String, Collection<String>> newChatView) {
        this.newChatView = newChatView;
    }

    private final Map<String, Collection<String>> newChatView;

    public Map<String, Collection<String>> newChatView() {
        return newChatView;
    }
}
