package channel;

import chat.State;
import com.google.common.eventbus.EventBus;
import com.google.protobuf.InvalidProtocolBufferException;
import message.UserJoinMessage;
import message.UserQuitMessage;
import org.jgroups.Address;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatState;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.stream.Collectors;

import static pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction.parseFrom;

/**
 * Created by adakis on 2017-03-24.
 */
public class ManagementChannelReceiver extends ReceiverAdapter {

    private final State state;
    private final EventBus eventBus;

    public ManagementChannelReceiver(State state, EventBus eventBus) {
        this.state = state;
        this.eventBus = eventBus;
    }

    @Override
    public void viewAccepted(View newView) {
        state.retainOnly(
                newView.getMembers()
                        .stream()
                        .map(Address::toString)
                        .collect(Collectors.toList())
        );

        eventBus.post(
                new ChatViewChanged(state.channelsWithUsers())
        );
    }

    @Override
    public void receive(Message msg) {
        try {

            final ChatAction chatAction = parseFrom(msg.getBuffer());
            final String channel = chatAction.getChannel();
            final String nickname = chatAction.getNickname();

            if (chatAction.getAction() == ChatAction.ActionType.JOIN) {
                state.registerUser(channel, nickname);
                eventBus.post(
                        new UserJoinMessage(channel, nickname)
                );
            } else {
                state.unregisterUser(channel, nickname);
                eventBus.post(
                        new UserQuitMessage(channel, nickname)
                );
            }

            eventBus.post(
                    new ChatViewChanged(state.channelsWithUsers())
            );
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        final ChatState chatState = ChatOperationProtos.ChatState
                .newBuilder()
                .addAllState(state.allActions())
                .build();

        output.write(
                chatState.toByteArray()
        );
    }

    @Override
    public void setState(InputStream input) throws Exception {
        Collection<ChatAction> allActions = ChatState.parseFrom(input).getStateList();
        state.update(allActions);
    }
}