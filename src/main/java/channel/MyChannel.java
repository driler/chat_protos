package channel;

import chat.ChatManager;
import chat.MessageWriter;
import chat.State;
import com.google.common.eventbus.EventBus;
import org.jgroups.JChannel;
import org.jgroups.Receiver;
import user.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adakis on 2017-03-23.
 */
public class MyChannel {

    private ChatManager chatManager;
    private State state = new State();
    private final Map<String, MessageWriter> channelWriters = new HashMap<>();

    public MyChannel() {
    }

    public MyChannel(ChatManager chatManager) {
        this.chatManager = chatManager;
    }

    public void join(User user, String channelName, EventBus eventBus) throws Exception {
            JChannel channel = buildChannel(user.getNickname(), channelName, new ChannelReceiver(channelName, eventBus));
            channelWriters.put(channelName, new MessageWriter(channelName, eventBus));
            channel.connect(channelName);
            chatManager.sendJoinMessage(user.getNickname(), channelName);
            user.putChannel(channelName, channel);
    }

    private JChannel buildChannel(String channelName, String hostName, Receiver receiver) throws Exception {
        return JChannelFactory.channelFor(channelName, hostName, receiver);
    }

    private JChannel buildChannel(String channelName, Receiver receiver) throws Exception {
        return JChannelFactory.channelFor(channelName, receiver);
    }

    public ChatManager getChatManager() {
        return chatManager;
    }


    public State getState() {
        return state;
    }

    public Map<String,MessageWriter> getChannelWriters() {
        return channelWriters;
    }

    public void initMyChannel(User user, EventBus eventBus) throws Exception {
        final ManagementChannelReceiver managementChannelReceiver = new ManagementChannelReceiver(state, eventBus);
        JChannel managerChannel = buildChannel(user.getNickname(), managementChannelReceiver);
        managerChannel.connect("ChatManagement321123");
        managerChannel.getState(null, 0);
        chatManager = new ChatManager(managerChannel);
    }
}
