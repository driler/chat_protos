package channel;

import com.google.common.eventbus.EventBus;
import com.google.protobuf.InvalidProtocolBufferException;
import message.ChatMessage;
import org.jgroups.Address;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

/**
 * Created by adakis on 2017-03-24.
 */
public class ChannelReceiver extends ReceiverAdapter {

    private final String channel;
    private final EventBus eventBus;

    public ChannelReceiver(String channel, EventBus eventBus) {
        this.channel = channel;
        this.eventBus = eventBus;
        eventBus.register(this);
    }

    @Override
    public void receive(Message msg) {
        final Address messageSource = msg.getSrc();
        try {
            final ChatOperationProtos.ChatMessage message = ChatOperationProtos.ChatMessage.parseFrom(msg.getBuffer());
            eventBus.post(
                    new ChatMessage(channel, messageSource.toString(), message.getMessage())
            );
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }
}

