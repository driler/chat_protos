package chat;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import message.Message;

/**
 * Created by adakis on 2017-03-24.
 */
public class MessageWriter {

    private final String channelName;

    public MessageWriter(String channelName, EventBus eventBus) {
            this.channelName = channelName;
            eventBus.register(this);
        }

    @Subscribe
    public void on(Message message) {
        if (channelName.equals(message.getChannel())) {
            System.out.println("[" + channelName + "] " + message);
        }
    }
}
