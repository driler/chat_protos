package chat;

import channel.MyChannel;
import com.google.common.eventbus.EventBus;
import user.User;

import java.util.Scanner;

/**
 * Created by adakis on 2017-03-23.
 */
public class Main {

    public static void main(String [] args) throws Exception {
        MyChannel channel = new MyChannel();
        EventBus eventBus = new EventBus();

        System.setProperty("java.net.preferIPv4Stack", "true");

        System.out.println("Please insert your nickname:");
        Scanner scanner = new Scanner(System.in);
        User user = new User(scanner.nextLine());
        channel.initMyChannel(user, eventBus);

        while (true) {
            System.out.println("Do you want to: \n1 - join channel \n2 - send message \n3 - show channels");
            int i = scanner.nextInt();
            if (i == 1)
                user.joinChannel(eventBus, channel);

            else if (i == 2)
                user.sendMessage();

            else if (i == 3)
                user.showChannels(channel.getState());

            else {
                System.out.println("Wrong choice - ending...");
                user.cleanUp(channel);
                break;
            }
        }
    }


}